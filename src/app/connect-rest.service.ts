import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as $ from 'jquery';

@Injectable({
  providedIn: 'root'
})
export class ConnectRestService {

  constructor( private http : HttpClient) { }

  async getMethod(getMehtod){
    $('.loader').removeClass('hidden')
    console.log('getNationality')
    return new Promise((success, failed) =>{
      this.http.get('http://localhost:3000/'+ getMehtod).subscribe(
        data => {$('.loader').addClass('hidden'),success(data)},
        error => {$('.loader').addClass('hidden'),failed(error)}
      )
    }) 
  }

  async updateCaseList(data){

    return new Promise((success, failed)=>{
      $('.loader').removeClass('hidden')
      this.http.post('http://localhost:3000/caseList/',data).subscribe(
        data=>{ $('.loader').addClass('hidden'), success(data)},
        error=>{ $('.loader').addClass('hidden'), failed(error)}
      )
    })
  }
}
