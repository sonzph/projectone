import { TestBed } from '@angular/core/testing';

import { ConnectRestService } from './connect-rest.service';

describe('ConnectRestService', () => {
  let service: ConnectRestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConnectRestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
