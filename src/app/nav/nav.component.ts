import { Component, Injectable, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import * as $ from 'jquery'

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})

@Injectable()

export class NavComponent implements OnInit {

  constructor(private router : Router) { 
    /*
    this.isPerson = localStorage.isPerson?localStorage.isPerson:false;
    this.isAddress = localStorage.isAddress?localStorage.isAddress:false,
    this.isWork = localStorage.isWork?localStorage.isWork:false,
    this.isReview = localStorage.isReview?localStorage.isReview:false
    */
  }


  
  ngOnInit(): void {
    //this.setIsPerson()
  }

  
  public setIsPerson(){
    $('#personal>span.check').removeClass('hidden')
    $('#personal>span.circle').addClass('hidden')
    $('#personal').prop('disabled',false)
  }

  public setIsAddress(){
    $('#address>span.check').removeClass('hidden')
    $('#address>span.circle').addClass('hidden')
    $('#address').prop('disabled',false)
  }

  public setIsWork(){
    $('#work>span.check').removeClass('hidden')
    $('#work>span.circle').addClass('hidden')
    $('#work').prop('disabled',false)
  }

  public setIsSummary(){
    $('#summary>span.check').removeClass('hidden')
    $('#summary>span.circle').addClass('hidden')
    $('#summary').prop('disabled',false)
  }

  onPerson(){
    this.router.navigate(['personal-info'])
  }

  onAddress(){
    this.router.navigate(['address-info'])
  }

  onWork(){
    this.router.navigate(['work-info'])
  }

  onSummary(){
    this.router.navigate(['review'])
  }
}
