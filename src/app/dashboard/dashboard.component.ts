import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { ConnectRestService } from './../connect-rest.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor( 
    private router: Router, 
    private appComp : AppComponent,
    private conn : ConnectRestService
    ) { 
    this.appComp.setOnFlow(false)
  }
  userLogin : any
  noRecord : boolean
  data : any
  photo : any
  caseListDescription : string = "Retrieving data..."
  ngOnInit(): void {
    
    
    this.noRecord = true

    this.getCaseList()
    
    this.userLogin = JSON.parse(localStorage.getItem('userLogin'))
    this.photo = 'data:image/png;base64,' + this.userLogin.photo 
  }

  async getCaseList(){
    this.data = await this.conn.getMethod('caseList')
    if(this.data.length>0){
      this.noRecord = false
    }else{
      this.caseListDescription = "No record found."
    }
  }

  onSignOut(){
    this.router.navigate([''])
  }

  onCreate(){
    this.appComp.setOnFlow(true)
    this.router.navigate(['personal-info'])
  }

  onResume(){

  }

}
