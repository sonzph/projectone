import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NavComponent } from './../nav/nav.component';
@Component({
  selector: 'app-address-info',
  templateUrl: './address-info.component.html',
  styleUrls: ['./address-info.component.scss']
})
export class AddressInfoComponent implements OnInit {

  constructor(private router: Router, private formBuilder : FormBuilder, private nav : NavComponent) {
    this.nav.setIsPerson()
   }

  addressInfo = this.formBuilder.group({
    Block:['', Validators.required],
    Street: ['', Validators.required],
    UnitNumber: ['', Validators.required],
    Floor: ['', Validators.required],
    PostalCode: ['', Validators.required]
  })

  currentCase : any
  ngOnInit(): void {
    this.nav.setIsPerson()
    this.currentCase = JSON.parse(localStorage.personInfo)
  }

  onBack(){
    this.router.navigate(['personal-info'])
  }

  onSubmit(){
    this.currentCase.AddressInformation = this.addressInfo.value
    this.currentCase.LastViewedPage = 'address-info'
    localStorage.personInfo = JSON.stringify(this.currentCase)
    this.nav.setIsAddress()
    this.router.navigate(['work-info'])
  }
}
