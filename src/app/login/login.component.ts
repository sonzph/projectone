import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from './../../environments/environment';
import { AppComponent } from '../app.component';
import { ConnectRestService } from './../connect-rest.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor( private router : Router, private appCom : AppComponent, private conn : ConnectRestService) { 
    this.appCom.onFlow = false
  }

  withError: boolean

  errorDescription: string

  UserID = new FormControl('')
  Password = new FormControl('')

  userList : any

  ngOnInit(): void {
    this.setDefault()
    this.getUserList()
  }

  async getUserList(){
    this.userList = await this.conn.getMethod('userList')
  }
  onSubmit(){
    console.log('onSubmit')
    for(var i = 0; i < this.userList.length; i++){
      if (this.userList[i].UserID == this.UserID.value && this.userList[i].Password == this.Password.value){
        if(this.userList[i].Deny == "N"){
          localStorage.setItem('userLogin', JSON.stringify({ name: this.userList[i].Name, photo: this.userList[i].Photo, userid: this.userList.UserID})) 
          this.router.navigate(['dashboard'])
          return
        }else{
          //testing
          this.withError = true
          this.errorDescription = "Your ID is temporary suspended. Please ask your system administrator."
          return
        }
      }
    }
    this.errorDescription = "Invalid credentials. Please try again."
    this.withError = true
  }

  setDefault(){
    this.withError = false;
  }
}
