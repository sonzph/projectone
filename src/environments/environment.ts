// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  userList : [{"UserID":"sonny","Deny":"N","Password":"4414","Name":"Sonny Torres"},{"UserID":"earl","Deny":"N","Password":"1234","Name":"Earl Don Dela Rosa"},{"UserID":"howard","Deny":"N","Password":"1234","Name":"Howard Pecson"}],

  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
