import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NavComponent } from './../nav/nav.component';
@Component({
  selector: 'app-work-info',
  templateUrl: './work-info.component.html',
  styleUrls: ['./work-info.component.scss']
})
export class WorkInfoComponent implements OnInit {

  constructor(private router : Router, private formBuilder: FormBuilder, private nav : NavComponent) { }

  workInfo = this.formBuilder.group({
    CompanyName : ['', Validators.required],
    ContactNumber  : ['', Validators.required],
    Position  : ['', Validators.required],
    YearOfService  : ['', Validators.required]
  })

  currentCase : any

  ngOnInit(): void {
    this.nav.setIsPerson()
    this.nav.setIsAddress()
    this.currentCase = JSON.parse(localStorage.personInfo)
  }

  onBack(){
    this.router.navigate(['address-info'])
  }

  onSubmit(){
    this.currentCase.LastViewedPage = 'work-info'
    this.currentCase.WorkInformation = this.workInfo.value
    localStorage.personInfo = JSON.stringify(this.currentCase)
    this.nav.setIsWork()
    this.router.navigate(['review'])
  }
}
