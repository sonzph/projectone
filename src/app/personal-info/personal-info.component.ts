import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { NavComponent } from '../nav/nav.component';

import { ConnectRestService } from './../connect-rest.service'

@Component({
  selector: 'app-personal-info',
  templateUrl: './personal-info.component.html',
  styleUrls: ['./personal-info.component.scss']
})
export class PersonalInfoComponent implements OnInit {

  
  constructor(
    private router : Router, 
    private formBuilder : FormBuilder,  
    private appComp: AppComponent, 
    private nav: NavComponent, 
    private conn : ConnectRestService,
    private datePipe : DatePipe) {
    localStorage.onFlow=true
    
   }

  personalInfo = this.formBuilder.group({
    GivenName: ['', Validators.required],
    LastName: ['', Validators.required],
    DOB: ['', Validators.required],
    Gender: ['', Validators.required],
    Nationality: ['', Validators.required],
    IDType: ['', Validators.required],
    IDNumber: ['', Validators.required],
    ExpDate: ['', Validators.required]
  })


  NationalityList:any
  User:any
  ngOnInit(): void {
    this.getNationality()
    this.User = JSON.parse(localStorage.userLogin)
  }

  async getNationality(){
    this.NationalityList = await this.conn.getMethod('nationality')
  }

  onBack(){
    this.router.navigate(['dashboard'])
    localStorage.removeItem('onFlow')
  }

  onSubmit(){
    let currentDate = new Date()
    let newCase = {CaseID: this.datePipe.transform(currentDate, 'yyyyMMddhhmmss'),PersonalInformation:{},Status:"Pending", LastViewedPage: "personal-info", CreatedBy: this.User.userid}
    newCase.PersonalInformation = this.personalInfo.value
    localStorage.personInfo = JSON.stringify(newCase)
    this.nav.setIsPerson()
    this.router.navigate(['address-info'])
  }
}
