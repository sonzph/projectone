import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PersonalInfoComponent } from './personal-info/personal-info.component';
import { WorkInfoComponent } from './work-info/work-info.component';
import { AddressInfoComponent } from './address-info/address-info.component';
import { SummaryComponent } from './summary/summary.component';

const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'personal-info', component: PersonalInfoComponent},
  {path: 'address-info', component: AddressInfoComponent},
  {path: 'work-info', component: WorkInfoComponent},
  {path: 'review', component: SummaryComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
