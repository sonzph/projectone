import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavComponent } from './../nav/nav.component';
import { ConnectRestService } from './../connect-rest.service';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {

  constructor(
    private router: Router,
    private nav : NavComponent,
    private conn : ConnectRestService,
    private datePipe : DatePipe
    ) { }
  currentCase : any
  ngOnInit(): void {
    this.nav.setIsPerson()
    this.nav.setIsAddress()
    this.nav.setIsWork()
    this.currentCase = JSON.parse(localStorage.personInfo)
  }

  onSubmit(){
    this.currentCase.LastViewedPage = 'review'
    this.currentCase.Status = 'Completed'
    //localStorage.removeItem('personInfo')
    this.updateCaseList()
  }

  async updateCaseList(){
    let response: any
    const currentDate = new Date()
    this.currentCase.DateSubmited = this.datePipe.transform(currentDate,"dd MMM yyyy")
    response =  await this.conn.updateCaseList(this.currentCase)
    console.log('response: ' + JSON.stringify(response))
    if(response.id){
      this.router.navigate(['dashboard'])
    }else{
      console.log('response: ' + JSON.stringify(response))
    }
  }
}
