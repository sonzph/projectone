import { Component } from '@angular/core';
import { NavComponent } from './nav/nav.component';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'project1';

  constructor(private nav : NavComponent) { }

  onFlow : boolean

  ngOnInit(): void {
    this.onFlow = localStorage.onFlow?localStorage.onFlow:false;
  }

  
  setOnFlow(val){
    this.onFlow = val;
  }

}
